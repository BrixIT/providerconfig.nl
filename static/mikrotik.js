function _generateLine(line) {
    var result = '';
    result += line[0];
    for (var fieldName in line[1]) {
        if (line[1].hasOwnProperty(fieldName)) {
            result += ' ' + fieldName + '=' + line[1][fieldName]
        }
    }
    return result + "\n";
}

function generateConfig(sections) {
    var result = '';
    for (var sectionName in sections) {
        if (sections.hasOwnProperty(sectionName)) {
            var section = sections[sectionName];

            result += '/' + sectionName + "\n";

            for (var i = 0; i < section.length; i++) {
                result += _generateLine(section[i]);
            }

            result += "\n";
        }
    }

    return result;
}

function _normalize_mac(raw) {
    var uc = raw.toUpperCase();
    uc = uc.replace(/\W/ig, '');
    return uc;
}

function mac_to_uc_dashed(raw) {
    var result = _normalize_mac(raw);
    result = result.replace(/(.{2})/g, "$1-");
    return result.slice(0, 17);
}