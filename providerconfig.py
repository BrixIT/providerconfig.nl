import os
from flask import Flask, render_template
import configparser

app = Flask(__name__)


def read_dataset(filename='dataset.ini'):
    config = configparser.ConfigParser()
    config.optionxform = str
    if not os.path.isfile(filename):
        raise Exception("{} not found".format(filename))
    config.read(filename)
    return config


@app.route('/')
def index():
    dataset = read_dataset()
    providers = []
    for item in dataset.sections():
        providers.append({
            'code': item,
            'provider': dataset.get(item, 'provider'),
            'netwerk': dataset.get(item, 'netwerk'),
            'medium': dataset.get(item, 'medium')
        })
    return render_template('home.html', providers=providers)


@app.route('/ftth-coax')
def ftth_coax():
    return render_template('ftth-coax.html')


@app.route('/config/<code>/')
def show_config(code):
    dataset = read_dataset()
    info = dict(dataset.items(code))
    stripped = {key: value.strip() for key, value in info.items()}

    return render_template('config.html', code=code, info=stripped)


@app.route('/benchmarks')
def benchmarks():
    benchmarks = read_dataset('benchmarks.ini')
    return render_template('benchmarks.html', benchmarks=benchmarks)


if __name__ == '__main__':
    app.run()
