from flask_frozen import Freezer
from providerconfig import app
from configparser import ConfigParser

freezer = Freezer(app)
app.config['FREEZER_DESTINATION'] = 'www'
app.config['FREEZER_BASE_URL'] = 'https://providerconfig.nl/'


@freezer.register_generator
def show_config():
    config = ConfigParser()
    config.read('dataset.ini')
    for code in config.sections():
        yield {'code': code}


if __name__ == '__main__':
    freezer.freeze()
