# ProviderConfig

This is the code for generating https://providerconfig.nl/

## Build

```shell-session
$ pip3 install -r requirements.txt
$ python3 build.py
$ xdg-open www/index.html
```